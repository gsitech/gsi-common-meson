#!/usr/bin/env bash

if [[ "$DEBUG_MODE" =~ ^([tT]([rR][uU][eE])?|[yY]([eE][sS])?|[oO][nN]|[1-9][0-9]*)$ ]]; then
    set -x
fi

EXIT_SUCCESS=0
EXIT_INVALID_OPTION=1
EXIT_COMMAND_NOT_FOUND=2
EXIT_MISSING_OPTION=3
EXIT_COMMAND_FAILED=4

declare PRINT_HELP
declare EXIT_HELP

declare BELEX_INPUT
declare BELEX_SOURCE
declare -a BELEX_ARGS
declare -a PREPROC_ARGS

function print-help() {
    cat <<"EOF"
Generates Belex sources and passes them through apl_preproc to bypass meson's
inability to compose generators
(see: https://github.com/mesonbuild/meson/issues/1141).

Options:
  -h|--help              Print this help text, then exit.
  --belex-input FILE     Path to the Belex (Python) input file used to generate
                         the .apl and .apl.h files.
  --belex-source FILE    Path to the file where the Belex APL source should be
                         generated.
  --belex-header FILE    Path to the file where the Belex APL header should be
                         generated.
  --belex-config FILE    Path to the Belex config file (optional).
  (*)                    Everything else is passed to belex_aot as arguments.

Usage: belex-preproc [--help] \
                     --belex-input path/to/belex_input.py \
                     --belex-source path/to/belex_source.apl \
                     --belex-header path/to/belex_source.apl.h \
                     [--belex-config path/to/belex_config.yaml] \
                     (*)
EOF
}

function parse-opts() {
    local RETURN_CODE=$EXIT_SUCCESS
    local OPTION

    while (( $# > 0 )); do
        OPTION="$1"
        case "$OPTION" in
            -h|--help)
                PRINT_HELP=true
                if [ -z "$EXIT_HELP" ]; then
                    EXIT_HELP=$EXIT_SUCCESS
                fi
                shift
                ;;
            --belex-input)
                if [ -z "$BELEX_INPUT" ]; then
                    BELEX_INPUT="$2"
                else
                    echo "$OPTION already set: $BELEX_INPUT" 1>&2
                    RETURN_CODE=$EXIT_INVALID_OPTION
                fi
                shift 2
                ;;
            --belex-source)
                if [ -z "$BELEX_SOURCE" ]; then
                    BELEX_SOURCE="$2"
                    BELEX_ARGS=( "${BELEX_ARGS[@]}" --source-file "$2" )
                else
                    echo "$OPTION already set: $BELEX_SOURCE" 1>&2
                    RETURN_CODE=$EXIT_INVALID_OPTION
                fi
                shift 2
                ;;
            --belex-header)
                BELEX_ARGS=( "${BELEX_ARGS[@]}" --header-file "$2" )
                shift 2
                ;;
            --belex-config)
                BELEX_ARGS=( "${BELEX_ARGS[@]}" --config "$2" )
                shift 2
                ;;
            *)
                # Everything else is passed to apl_preproc
                PREPROC_ARGS=( "${PREPROC_ARGS[@]}" "$OPTION" )
                shift
                ;;
        esac
    done

    if [ -z "$BELEX_INPUT" ]; then
        echo "Required argument not specified: --belex-input" 1>&2
        RETURN_CODE=$EXIT_MISSING_OPTION
    fi

    return $RETURN_CODE
}

function command-exists() {
    local COMMAND="$1"
    command -v "$COMMAND" &>/dev/null
    return $?
}

function find-commands() {
    local RETURN_CODE=$EXIT_SUCCESS
    local COMMAND

    for COMMAND in belex-aot apl_preproc; do
        if ! command-exists "$COMMAND"; then
            echo "Required command not found: $COMMAND" 1>&2
            RETURN_CODE=$EXIT_COMMAND_NOT_FOUND
        fi
    done

    return $RETURN_CODE
}

function run-command() {
    local RETURN_CODE=$EXIT_SUCCESS
    "$@"
    RETURN_CODE=$?
    if (( RETURN_CODE != EXIT_SUCCESS )); then
        echo "Command failed: $*" 1>&2
        RETURN_CODE=$EXIT_COMMAND_FAILED
    fi
    return $RETURN_CODE
}

function generate-belex-sources() {
    local RETURN_CODE
    run-command belex-aot \
        --enable-optimization eliminate-read-after-write \
        --enable-optimization delete-dead-writes \
        --enable-optimization replace-zero-xor \
        --no-uniquify-nyms \
        "${BELEX_ARGS[@]}" \
        "$BELEX_INPUT"
    RETURN_CODE=$?
    return $RETURN_CODE
}

function preproc-belex-sources() {
    local RETURN_CODE
    run-command apl_preproc \
                "${PREPROC_ARGS[@]}" \
                "$BELEX_SOURCE"
    RETURN_CODE=$?
    return $RETURN_CODE
}

function main() {
    local RETURN_CODE

    parse-opts "$@"
    RETURN_CODE=$?

    if [ -n "$PRINT_HELP" ]; then
        print-help
        return $EXIT_HELP
    fi

    if (( RETURN_CODE == EXIT_SUCCESS )); then
        find-commands
        RETURN_CODE=$?
    fi

    if (( RETURN_CODE == EXIT_SUCCESS )); then
        generate-belex-sources
        RETURN_CODE=$?
    fi

    if (( RETURN_CODE == EXIT_SUCCESS )); then
        preproc-belex-sources
        RETURN_CODE=$?
    fi

    return $RETURN_CODE
}

main "$@"
exit $?
